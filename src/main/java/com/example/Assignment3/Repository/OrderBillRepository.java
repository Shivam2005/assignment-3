package com.example.Assignment3.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.Assignment3.Entity.OrderBill;

// Order Bill repository extending JPA repository

public interface OrderBillRepository extends JpaRepository<OrderBill, Integer>{

}
