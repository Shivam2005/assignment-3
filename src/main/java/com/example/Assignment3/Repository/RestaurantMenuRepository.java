package com.example.Assignment3.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.Assignment3.Entity.RestaurantMenu;

//Restaurant menu repository extending JPA repository

public interface RestaurantMenuRepository extends JpaRepository<RestaurantMenu, Integer> {

}
