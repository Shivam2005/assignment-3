# Surabi SQL file is added in the repository

# Postman Collection Link is here: (All the API's are saved here)
https://www.getpostman.com/collections/7ef364b28257f40f293a

# After registering , updating , deleting a User please check API route to get all users to check these operations are applied or not.

# API's routes for Admin are:

1. Get all users
2. Delete a user
3. Update a user
4. Register a user
5. Get one user
6. Delete all user
7. Get all admins
8. Admin login
9. Admin logout
10. Todays all bills
11. Monthly all bills

# API's routes for User are:

1. Login user
2. Logout user
3. Restaurant menu
4. Buy items
5. User Bill