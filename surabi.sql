/*Creating surabi restaurant database*/

create database surabi;
use surabi;


/*Creating Admin table */

create table admin(
    adminId int ,
    adminName char (50),
    adminPassword varchar(50)
);

/* Valid Admins for restaurant surabi */

insert into admin values('1','Admin','Admin',false);
insert into admin values('2','Accolite','Accolite',false);


/* Creating User table*/

create table user(
	userId int ,
    userName char (50),
    userPassword varchar(50)    
);

/* Valid Users table*/

insert into user values('1',false,'Shivam','Shivam');
insert into user values('2',false,'Amisha','Amisha');
insert into user values('3',false,'Prem','Prem');



/*Restaurant menu table*/

create table restaurantMenu(
	itemId int ,
    itemName char (40),
    cost int
);

/* Table for all items in the restaurant menu*/

insert into restaurant_menu values('1',30,'Aloo Paratha');
insert into restaurant_menu values('2',50,'Burger');
insert into restaurant_menu values('3',100,'Masala Chana');
insert into restaurant_menu values('4',70,'Dosa');
insert into restaurant_menu values('5',50,'Finger Chips');
insert into restaurant_menu values('6',60,'Veg roll');
insert into restaurant_menu values('7',300,'Pizza');
insert into restaurant_menu values('8',45,'Sandwich');
insert into restaurant_menu values('9',55,'Paneer Paratha');
insert into restaurant_menu values('10',80,'Hot Dog');



 