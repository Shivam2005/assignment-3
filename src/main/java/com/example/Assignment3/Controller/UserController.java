package com.example.Assignment3.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.Assignment3.Entity.User;
import com.example.Assignment3.Service.UserService;


@RestController
@RequestMapping("/api")

public class UserController {

	@Autowired
	private UserService service;
	
	
	//To get particular one user
	@GetMapping("/oneUser/{userId}")
	public Optional<User> getUser(@PathVariable("userId") int userId)  { 
		return service.getUserById(userId);  
	}  
	
	
	//To get all users from database
	@GetMapping("/getAll")
	public List<User> getAllUsers () throws Exception {
		System.out.println("Getting all user");
		return service.getAllUsers();
	}
	
	//To register a user in database
	@PostMapping("/register")
	public String registerUser(@Validated @RequestBody User newUser) throws Exception {
		return service.registerUser(newUser);
	}
	
	
	//Login method for user
	@PostMapping("/login")
	public String loginUser(@Validated @RequestBody User newUser) throws Exception {
		return service.logInUser(newUser);
	}
	
	
	//Logout method for user
	@GetMapping("/logout")
	public String logUserOut() throws Exception {
		return service.logUserOut();
	}
	
	//To update a particular user's username and password
	@PutMapping("/update")
	public User updateUser(@RequestBody User U) throws Exception {
		return service.updateUser(U);
	}
	
	//To delete a particular user using its id
	@DeleteMapping("/delete/{userId}")
	public  String deleteUser(@PathVariable("userId")  int userId) throws Exception {
		return service.deleteUser(userId);
	}
	
	
	//To delete all users form database
	@DeleteMapping("/deleteAll")
	public String deleteAll() throws Exception {
        return service.deleteAll();
	}
}
