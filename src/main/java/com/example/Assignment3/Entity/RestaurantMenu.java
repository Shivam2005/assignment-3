package com.example.Assignment3.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//Restaurant menu entity with its getters,setters

@Entity
public class RestaurantMenu {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="itemId")
	int itemId;
	@Column(name="itemName")
	String itemName;
	@Column(name="cost")
	int cost;
	
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	
	
}
