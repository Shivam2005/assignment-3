package com.example.Assignment3.Entity;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

// Order Bill entity with its getters,setters and tostring method

@Component
@Entity
@Table
public class OrderBill {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int orderID;
	@Column(name="orderDate")
	private LocalDate orderDate = LocalDate.now();
	@Column(name="itemId")
	private int itemId;
	@Column(name="itemCost")
	private int itemCost;
	
	public OrderBill() {}
	
	public OrderBill(int orderID, LocalDate orderDate, int itemId, int itemCost) {
		this.orderID = orderID;
		this.orderDate = orderDate;
		this.itemId = itemId;
		this.itemCost = itemCost;
	}
	
	public int getOrderID() {
		return orderID;
	}
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}
	public LocalDate getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public int getItemCost() {
		return itemCost;
	}
	public void setItemCost(int itemCost) {
		this.itemCost = itemCost;
	}
	@Override
	public String toString() {
		return "OrderBill [orderID=" + orderID + ", orderDate=" + orderDate + ", itemId=" + itemId + ", itemCost="
				+ itemCost + "]";
	}
	
	
	
}
