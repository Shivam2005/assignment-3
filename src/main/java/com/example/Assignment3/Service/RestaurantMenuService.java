package com.example.Assignment3.Service;
import java.time.LocalDate; 
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.Assignment3.Entity.OrderBill;
import com.example.Assignment3.Entity.RestaurantMenu;

import com.example.Assignment3.Repository.OrderBillRepository;
import com.example.Assignment3.Repository.RestaurantMenuRepository;


// This service will talk to restaurnat_menu_repository

@Service
public class RestaurantMenuService {

	@Autowired
	private  RestaurantMenuRepository  repository;
	@Autowired
	private OrderBillRepository orderRepository;
     

	public static int Cost=0;
	public static String printItem="";
	
	
	//get menu list of restaurant
	public List<RestaurantMenu> getAllItems() throws Exception{
		try {
			return repository.findAll();
		} catch (Exception e) {
            throw new Exception("Error in printing items");
		}
		
	}

	@Autowired
	private OrderBill bill;
	
	//User can buy one/many items
	
	public String buyItems(int id) throws Exception {
		try {
			RestaurantMenu item = repository.getById(id);
			
			Cost+=item.getCost();
			printItem+= "[ Date: "+LocalDate.now()+" | Item: "+item.getItemName()+" | "+" Cost: "+item.getCost()+ " ]\n";
			
			bill = new OrderBill ( 0,LocalDate.now(),id,item.getCost() );
			
			orderRepository.save(bill);
			
			return "Item added successfully...\nWant to buy more item, go to same api again\nOtherwise, go fo bill api\nThank You";
		
		} catch (Exception e) {
            throw new Exception("Error in buying items");
		 
		}
	}	
	
	//Printing Bill with cost and items
	public String getBill() throws Exception {
		try {
			return printItem+"\nTotal Cost: "+Cost;
		} catch (Exception e) {
			throw new Exception("Error in total cost");
		}
		
	}
}
