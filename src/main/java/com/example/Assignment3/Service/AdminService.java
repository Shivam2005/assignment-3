package com.example.Assignment3.Service;

import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.Assignment3.Entity.Admin;
import com.example.Assignment3.Entity.OrderBill;
import com.example.Assignment3.Entity.User;
import com.example.Assignment3.Repository.AdminRepository;
import com.example.Assignment3.Repository.OrderBillRepository;

//Admin service where all the methods are initialized

@Service
public class AdminService {

	@Autowired
	private AdminRepository repository;
	@Autowired
	private OrderBillRepository orderRepository;
	
	private String printBill="";
	
	public List<Admin> getAllAdmin() {
		return repository.findAll();
	}
	
	@Autowired
	private Admin admin;
	
	//Login method for admin
	public String loginAdmin(Admin newAdmin) throws Exception {
		try {
			List<Admin> admins = repository.findAll();
	        for (Admin other : admins) {
	            if (other.getAdminName().equals(newAdmin.getAdminName()) && other.getAdminPassword().equals(newAdmin.getAdminPassword())) {
	                newAdmin.setLoggedIn(true);
	                admin = newAdmin;
	                return "Admin Login success";
	            }
	        }
		} catch (Exception e) {
			throw new Exception("Admin Login error");
		}
			
        
       return "Admin Login failure";
    }
	
	
	//logout method for admin
	public String logAdminOut() throws Exception {
		try {
			  if(admin==null) {
		        	return "Login first";
		        }
		        else {
		        	if(admin.isLoggedIn()) {
		        		admin.setLoggedIn(false);
		        		return "Logout successfully";
		        	} 	
		        }
		} catch (Exception e) {
			throw new Exception("Login First");
		}
      
		return "Login first";   	
    }
	
	// Generating bill for today
	public String todayBill() throws Exception {
		try {
			List<OrderBill> orders = orderRepository.findAll();
			for(OrderBill other : orders) {
				if(other.getOrderDate().equals(LocalDate.now())) {
				   printBill+=other+"\n";	
				}
			}
			return "Todays all Bills:\n"+printBill;
		} catch (Exception e) {
			throw new Exception("Error in generating todays bill");
		}	
	}

	
	// Generating Monthly Bill
	public String monthBill() throws Exception {
		try {
			List<OrderBill> orders = orderRepository.findAll();
			printBill="";
			for(OrderBill other : orders) {
				if(other.getOrderDate().getMonth().equals(LocalDate.now().getMonth())) {
				   printBill+=other+"\n";	
				}
			}
			return "Monthly all Bills:\n"+printBill;
		} catch (Exception e) {
			throw new Exception("Error in monthly bill printing");
		}
		
	}
}
