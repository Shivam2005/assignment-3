//package com.example.Assignment3.spring.AOP;
//
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Pointcut;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;
//import com.fasterxml.jackson.databind.ObjectMapper;
//
////Logging Mechanism
//
//@Aspect
//@Component
//public class Advice {
//	
//	Logger log = LoggerFactory.getLogger(Advice.class);
//	
//	@Pointcut(value = "Execution( * com.example.Assignment3.*.*.*(..))")
//	public void myPointcut() {
//		
//	}
//	
//	@Around("myPointCut")
//	public Object logger(ProceedingJoinPoint pp) throws Throwable {
//		ObjectMapper mapper = new ObjectMapper();
//		String methodName = pp.getSignature().getName();
//		String className = pp.getTarget().getClass().toString();
//		Object [] myArr = pp.getArgs();
//		
//		//Before Advice is used here...
//		//Printing class name, method name and arguments that are passes to it...
//		log.info("Method Called "+ className+": " +methodName+"()"+ "All Arguments: "+mapper.writeValueAsString(myArr));
//		
//		Object obj = pp.proceed();
//		
//		//Printing class name, method name and responses that we recived...
//		log.info(className+": " +methodName+"()"+ "Response: "+mapper.writeValueAsString(obj));
//
//		return obj;
//	}
//	
//}
