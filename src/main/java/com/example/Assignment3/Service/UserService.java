package com.example.Assignment3.Service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import com.example.Assignment3.Entity.User;
import com.example.Assignment3.Repository.AdminRepository;
import com.example.Assignment3.Repository.UserRepository;

//User service where all the methods are initialized

@Service
public class UserService {

	@Autowired
	private UserRepository repository;
	

	//Updating a user 
	public User updateUser(User U) throws Exception{
		try {
			  return repository.save(U);	
		} catch (Exception e) {
			  throw new Exception("Error in updating user");
		}
      
	}
	
	
	//Registering a new user using its username and password
	public String registerUser(User newUser) throws Exception {
		try {
			List<User> users = repository.findAll();
	        for (User user : users) {
	            if (user.equals(newUser)) {
	            	return "User Already exists!";
	            }
	        }
	       repository.save(newUser);
	       return "User registered Successfully";
		
		} catch (Exception e) {
			throw new Exception("Error in registraion");
		}     
	}	
	//Get a particular user using its id
	public Optional<User> getUserById(int userId) {
		return repository.findById(userId);
	}
	
	
	@Autowired
	private User user;
	
	
	//Login method for user
	public String logInUser(User newUser) throws Exception {
		try {
			List<User> users = repository.findAll();
	        for (User other : users) {
	            if (other.getUserName().equals(newUser.getUserName()) && other.getUserPassword() .equals(newUser.getUserPassword())) {
	            	newUser.setLoggedIn(true);
	            	user = newUser;
	            	RestaurantMenuService.printItem="";
	            	RestaurantMenuService.Cost=0;
	                return "Log in success";
	            }
	        }
       return "Login failure";
		} catch (Exception e) {
			throw new Exception("Error in login");
		}
			
    }
	
	//Logout method for user
	public String logUserOut() throws Exception {
		try {
			if(user==null) {
	        	return "Login first";
	        }
	        else {
	        	if(user.isLoggedIn()) {
	        		user.setLoggedIn(false);
	        		return "Logout successfully";
	        	}
	        	
	        }
			return "Login first";   
		} catch (Exception e) {
			throw new Exception();
		}
        	
    }
	
	
	//Method to print all users from database surabi
	public List<User> getAllUsers() throws Exception{
		try {
			return repository.findAll();	
	
		} catch (Exception e) {
		throw new Exception("Error in printing all users");
		}
			}
		
	
	// To delete a user using its id
	public String deleteUser(int userId) throws Exception {
		try {
			 repository.deleteById(userId);
		} catch (Exception e) {
			throw new Exception("Error in deleting user");
		}
		
		return "User deleted Successfully...";
	}
	
	// To delete all users from database
	public String deleteAll() throws Exception {
		try {
			repository.deleteAll();
			return "All User deleted Successfully...";
		} catch (Exception e) {
			throw new Exception("Error in deleting all user");
		}
		
	}
}
