package com.example.Assignment3.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.Assignment3.Entity.RestaurantMenu;
import com.example.Assignment3.Service.RestaurantMenuService;



@RestController
@RequestMapping("/user")
public class RestaurantMenuController {

	@Autowired
	private RestaurantMenuService service;
	
	
	// To get restaurant menu by user
	@GetMapping("/menu")
	public List<RestaurantMenu> getAllItems() throws Exception{
		return service.getAllItems();
	}
	
	
	// User can buy items using item id
    @GetMapping("/item/{itemId}")
    public String buyItem(@PathVariable ("itemId") int itemId) throws Exception {
    	return service.buyItems(itemId);
    }
    
    
    //User can see his bill using below api route
    @GetMapping("/itemBill")
    public String itemBill() throws Exception{
    	return service.getBill();
    }
    
}
