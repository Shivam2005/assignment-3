package com.example.Assignment3.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Assignment3.Entity.Admin;
import com.example.Assignment3.Service.AdminService;

@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private AdminService service;
	
	
	//Login method for admin
	@PostMapping("/login")
	public String loginAdmin(@Validated @RequestBody  Admin newAdmin) throws Exception {
		return service.loginAdmin(newAdmin);
	}
	
	
	//Logout method for admin
	@GetMapping("/logout")
	public String logAdminOut() throws Exception {
		return service.logAdminOut();
	}

	//Today's all biils generated
	@GetMapping("/todayBill")
	public String todayBill() throws Exception {
		return service.todayBill();
	}
	
	
	//Monthly all bills generated
	@GetMapping("/monthBill")
	public String monthBill() throws Exception {
		return service.monthBill();
	}
	
	
	//To get all Admins of restaurant surabi from database surabi
	@GetMapping("/getAll")
	public List<Admin> getAllAdmin() {
		return service.getAllAdmin();
	}
}
