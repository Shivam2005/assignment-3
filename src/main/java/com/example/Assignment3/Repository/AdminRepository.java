package com.example.Assignment3.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.Assignment3.Entity.Admin;


// Admins repository extending JPA repository
@Repository
public interface AdminRepository extends JpaRepository<Admin, String> {

}
